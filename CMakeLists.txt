#
# CMakeLists.txt for LillyDAP
#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>
#
cmake_minimum_required (VERSION 3.18 FATAL_ERROR)
project ("LillyDAP" VERSION 1.0.1 LANGUAGES C)

set (CMAKE_C_STANDARD 99)
set (CMAKE_C_STANDARD_REQUIRED ON)

option (BUILD_SINGLE_THREADED "Build without atomic operations because LillyDAP runs in one thread" OFF)

include (FeatureSummary)
find_package (ARPA2CM 1.0 NO_MODULE)
set_package_properties (ARPA2CM PROPERTIES
    DESCRIPTION "CMake modules for ARPA2 projects"
    TYPE REQUIRED
    URL "https://gitlab.com/arpa2/arpa2cm/"
    PURPOSE "Required for the CMake build system of LillyDAP"
)

if (ARPA2CM_FOUND)
    set (CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${ARPA2CM_MODULE_PATH}
    )
else()
    feature_summary (WHAT ALL)
    message (FATAL_ERROR "ARPA2CM is required.")
endif()

include (MacroEnsureOutOfSourceBuild)
include (MacroAddUninstallTarget)
include (MacroGitVersionInfo)
include (MacroCreateConfigFiles)
include (MacroLibraryPair)

macro_ensure_out_of_source_build("Do not build LillyDAP in the source directory.")
get_project_git_version()

#
# Dependencies
#
find_package(Gperf)
set_package_properties(Gperf PROPERTIES
    TYPE REQUIRED
    PURPOSE "Hash function for internal tables"
)

if (NOT ${BUILD_SINGLE_THREADED})
    find_package (Threads)
    set_package_properties(Threads PROPERTIES
        DESCRIPTION "Threading library"
        TYPE REQUIRED
        PURPOSE "Multi-threaded LillyDAP"
    )
endif()

find_package (Quick-DER 1.7.0)
set_package_properties(Quick-DER PROPERTIES
    DESCRIPTION "Quick-and-easy DER"
    TYPE REQUIRED
    PURPOSE "LDAP data decoding"
)

feature_summary(FATAL_ON_MISSING_REQUIRED_PACKAGES WHAT ALL)

#
# Building
#

include_directories (include)

if (${BUILD_SINGLE_THREADED})
    add_definitions (-DCONFIG_SINGLE_THREADED)
else ()
    add_subdirectory (import)
    get_directory_property (OPENPA_INCLUDE_DIRS DIRECTORY import DEFINITION OPENPA_INCLUDE_DIRS)
    include_directories (${OPENPA_INCLUDE_DIRS})
endif ()

add_subdirectory (lib)

#
# TESTS
#

enable_testing ()
add_subdirectory (test)

#
# INSTALLING
#

install (
    DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/lillydap
    DESTINATION include
    FILES_MATCHING PATTERN "*.h"
)


#
# PACKAGING
#

set (CPACK_BUNDLE_NAME "LillyDAP")
set (CPACK_PACKAGE_CONTACT "Rick van Rein <rick@openfortress.nl>")
set (CPACK_PACKAGE_VENDOR "ARPA2.net")
set (CPACK_PACKAGE_DESCRIPTION_SUMMARY "LDAP devkit, turns LDAP operations into functions/callbacks")
set (CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/README.MD)
set (CPACK_RPM_PACKAGE_LICENSE "BSD-2-Clause") # SPDX-Identifier, used by RPM and FreeBSD Generators

include(PackAllPossible)
include (CPack)

create_config_files (LillyDAP EXPORT)
